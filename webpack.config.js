const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');//склейка подключаемых массивов и объектов настрокий вэбпака
const pug = require('./webpack/pug');//шаблонизатор
const devserver = require('./webpack/devserver');//сервер наблюдения при разработке
const sass = require('./webpack/sass'); //обработка сасс
const css = require('./webpack/css'); //нормалайз css
const extractCSS = require('./webpack/css.extract');//подгружаем самодостаточный моудль выгрузки css только для буилд
const uglifyJS = require('./webpack/js.uglify');//минификация js
const images = require('./webpack/images');//подгрузка картинок
const bootstrapEntryPoints = require('./webpack/webpack.bootstrap.config.js');
const fonts = require('./webpack/fonts.js'); //fonts and icons

const PATHS = {
    source: path.join(__dirname, 'source'),
    build: path.join(__dirname, 'build'),
    img: path.join(__dirname, 'img')
};

console.log('===============================================');
console.log('=======================>', process.argv[3], '<==========');
console.log('===============================================');
var bootstrapConfig = (process.argv[3] === 'development') ? bootstrapEntryPoints.dev : bootstrapEntryPoints.prod;

const common = merge([
  {
    entry: {
        'index': PATHS.source + '/pages/index/index.js',
        'usr_order_fast': PATHS.source + '/pages/usr_order_fast/usr_order_fast.js',
        'mg_order_call': PATHS.source + '/pages/mg_order_call/mg_order_call.js',
        'mg_order_mail': PATHS.source + '/pages/mg_order_mail/mg_order_mail.js',
        'usr_order_mail': PATHS.source + '/pages/usr_order_mail/usr_order_mail.js',
        'mg_payment_success': PATHS.source + '/pages/mg_payment_success/mg_payment_success.js',
        'usr_payment_deny': PATHS.source + '/pages/usr_payment_deny/usr_payment_deny.js',
        'usr_payment_success': PATHS.source + '/pages/usr_payment_success/usr_payment_success.js',
        'usr_order_cancel': PATHS.source + '/pages/usr_order_cancel/usr_order_cancel.js',
        'mg_order_basket': PATHS.source + '/pages/mg_order_basket/mg_order_basket.js',
        'usr_order_basket': PATHS.source + '/pages/usr_order_basket/usr_order_basket.js',
        // 'mg_correspondence_mail': PATHS.source + '/pages/mg_correspondence_mail/mg_correspondence_mail.js',
        // 'usr_order_basket_forgot': PATHS.source + '/pages/usr_order_basket_forgot/usr_order_basket_forgot.js',
        // 'mg_order_constructor': PATHS.source + '/pages/mg_order_constructor/mg_order_constructor.js',
        // 'usr_order_constructor': PATHS.source + '/pages/usr_order_constructor/usr_order_constructor.js',
        // 'usr_give_feedback': PATHS.source + '/pages/usr_give_feedback/usr_give_feedback.js',
        // 'usr_interview': PATHS.source + '/pages/usr_interview/usr_interview.js'
        // 'bootstrap': bootstrapConfig
    },
    output: {
        path: PATHS.build,
        filename: './js/[name].js',
        library: 'myapp'
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
          compress: {
            warnings:false,
            drop_console:true,
            unsafe:true
          },
          minimize: true
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            chunks: ['index'],
            template: PATHS.source + '/pages/index/index.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'usr_order_fast.html',
            chunks: ['usr_order_fast'],
            template: PATHS.source + '/pages/usr_order_fast/usr_order_fast.pug'
        }),
        // new HtmlWebpackPlugin({
        //     filename: 'mg_correspondence_mail.html',
        //     chunks: ['mg_correspondence_mail'],
        //     template: PATHS.source + '/pages/mg_correspondence_mail/mg_correspondence_mail.pug'
        // }),
        new HtmlWebpackPlugin({
            filename: 'mg_order_call.html',
            chunks: ['mg_order_call'],
            template: PATHS.source + '/pages/mg_order_call/mg_order_call.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'mg_order_mail.html',
            chunks: ['mg_order_mail'],
            template: PATHS.source + '/pages/mg_order_mail/mg_order_mail.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'usr_order_mail.html',
            chunks: ['usr_order_mail'],
            template: PATHS.source + '/pages/usr_order_mail/usr_order_mail.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'mg_payment_success.html',
            chunks: ['mg_payment_success'],
            template: PATHS.source + '/pages/mg_payment_success/mg_payment_success.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'usr_payment_deny.html',
            chunks: ['usr_payment_deny'],
            template: PATHS.source + '/pages/usr_payment_deny/usr_payment_deny.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'usr_payment_success.html',
            chunks: ['usr_payment_success'],
            template: PATHS.source + '/pages/usr_payment_success/usr_payment_success.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'usr_order_cancel.html',
            chunks: ['usr_order_cancel'],
            template: PATHS.source + '/pages/usr_order_cancel/usr_order_cancel.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'mg_order_basket.html',
            chunks: ['mg_order_basket'],
            template: PATHS.source + '/pages/mg_order_basket/mg_order_basket.pug'
        }),
        new HtmlWebpackPlugin({
            filename: 'usr_order_basket.html',
            chunks: ['usr_order_basket'],
            template: PATHS.source + '/pages/usr_order_basket/usr_order_basket.pug'
        }),
        // new HtmlWebpackPlugin({
        //     filename: 'usr_order_basket_forgot.html',
        //     chunks: ['usr_order_basket_forgot'],
        //     template: PATHS.source + '/pages/usr_order_basket_forgot/usr_order_basket_forgot.pug'
        // }),
        // new HtmlWebpackPlugin({
        //     filename: 'mg_order_constructor.html',
        //     chunks: ['mg_order_constructor'],
        //     template: PATHS.source + '/pages/mg_order_constructor/mg_order_constructor.pug'
        // }),
        // new HtmlWebpackPlugin({
        //     filename: 'usr_order_constructor.html',
        //     chunks: ['usr_order_constructor'],
        //     template: PATHS.source + '/pages/usr_order_constructor/usr_order_constructor.pug'
        // }),
        // new HtmlWebpackPlugin({
        //     filename: 'usr_give_feedback.html',
        //     chunks: ['usr_give_feedback'],
        //     template: PATHS.source + '/pages/usr_give_feedback/usr_give_feedback.pug'
        // }),
        // new HtmlWebpackPlugin({
        //     filename: 'usr_interview.html',
        //     chunks: ['usr_interview'],
        //     template: PATHS.source + '/pages/usr_interview/usr_interview.pug'
        // }),
        // данный плагин может объеденять общие для шаблона js
        // new webpack.optimize.CommonsChunkPlugin({
        //   name: "common"
        // }),
        // new webpack.ProvidePlugin({
        //   jQuery: 'jquery',
        //   $: 'jquery',
        //   jquery: 'jquery',
        //   "window.jQuery": "jquery"
        // })
    ]
  },
  pug(),
  images(),
  fonts()
]);


/******************************************/
module.exports = function(env) {
    if (env === 'production') {
        return merge([
          common,
          extractCSS(),
          uglifyJS(),
          css()
        ]);
    }
    if (env === 'development') {
        return merge([
          common,
          devserver(),
          sass(),
          css()
          ]);
    }
};
